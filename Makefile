SRC= lex.cpp
BIN=lex
CXXFLAGS= -std=c++11 -O0 -ggdb3 -Wall -Wextra
CXX=g++
all: $(BIN)

$(BIN): $(SRC)
	$(CXX) $(CXXFLAGS) $(SRC) -o $(BIN)
gentest: gentest.cpp
	$(CXX) $(CXXFLAGS) gentest.cpp -o gentest
clean:
	rm $(BIN)
