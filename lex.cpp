#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <iostream>

template<class C>
struct poczatkowy_ident {
	typedef C sygnatura;
	sygnatura zawartosc;
	int pozycja;
	bool operator==(const poczatkowy_ident<C> &op) const;
	bool operator!=(const poczatkowy_ident<C> &op) const;
	bool operator<(const poczatkowy_ident<C> &op) const;
	bool operator>(const poczatkowy_ident<C> &op) const;
	bool operator<=(const poczatkowy_ident<C> &op) const;
	bool operator>=(const poczatkowy_ident<C> &op) const;
};

template<class C>
bool poczatkowy_ident<C>::operator<=(const poczatkowy_ident<C> &op) const
{
	return this->zawartosc <= op.zawartosc;
}

template<class C>
bool poczatkowy_ident<C>::operator>=(const poczatkowy_ident<C> &op) const
{
	return (op <= (*this));
}

template<class C>
bool poczatkowy_ident<C>::operator==(const poczatkowy_ident<C> &op) const
{
	return ((*this) >= op) && ((*this) <= op);
}

template<class C>
bool poczatkowy_ident<C>::operator!=(const poczatkowy_ident<C> &op) const
{
	return !((*this) == op);
}

template<class C>
bool poczatkowy_ident<C>::operator<(const poczatkowy_ident<C> &op) const
{
	return (*this) <= op && (*this) != op;
}

template<class C>
bool poczatkowy_ident<C>::operator>(const poczatkowy_ident<C> &op) const
{
	return op < *this;
}

const unsigned int PODSTAWA_LOGARYMU = 2;

unsigned int log_podloga(const unsigned int liczba)
{
	if (liczba < 2)
		return 0;
	return log(liczba) / log(PODSTAWA_LOGARYMU);
}

unsigned int potega(const unsigned int liczba)
{
	const unsigned int PODSTAWA = 2;
	return pow(PODSTAWA, liczba);
}

template<class C>
void przepisz_identyfikatory(poczatkowy_ident<C> *tmp,
			     unsigned int **tablica_identyfikatorow,
			     unsigned int dlugosc_slowa,
			     unsigned int &ostatni_zuzyty,
			     unsigned int ktory_wiersz)
{
	ostatni_zuzyty++;
	tablica_identyfikatorow[tmp[0].pozycja][ktory_wiersz] = ostatni_zuzyty;
	for (unsigned int i = 1; i < dlugosc_slowa; i++) {
		if (tmp[i] != tmp[i - 1]) {
			ostatni_zuzyty++;
		}
		tablica_identyfikatorow[tmp[i].pozycja][ktory_wiersz] =
			ostatni_zuzyty;
	}
}

void zrob_wiersz(unsigned int wiersz, unsigned int dlugosc_slowa,
		 unsigned int **tablica_identyfikatorow,
		 unsigned int &ostatni_zuzyty)
{
	const unsigned int DWA_DO_WIERSZ = potega(wiersz);
	// poczatek iteracji
	const unsigned int POCZATEK = 0;
	// koniec iteracji, bez tego indeksu!
	const unsigned int KONIEC = dlugosc_slowa - DWA_DO_WIERSZ + 1;
	poczatkowy_ident<std::pair<int, int> > *tmp =
		new poczatkowy_ident<std::pair<int, int> >[KONIEC];
	for (unsigned int i = POCZATEK; i < KONIEC; i++) {
		tmp[i].pozycja = i;
		tmp[i].zawartosc.first = tablica_identyfikatorow[i][wiersz - 1];
		tmp[i].zawartosc.second =
                     tablica_identyfikatorow[i + DWA_DO_WIERSZ / 2][wiersz - 1];
	}
	std::sort(tmp, tmp + KONIEC);
	przepisz_identyfikatory(tmp, tablica_identyfikatorow, KONIEC,
				ostatni_zuzyty, wiersz);
	delete[] tmp;
}

void przypisz_reszte_identyfikatorow(unsigned int **tablica_identyfikatorow,
				     unsigned int dlugosc_slowa,
				     unsigned int &ostatni_zuzyty)
{
	const unsigned int POCZATEK = 1;
	const unsigned int KONIEC = log_podloga(dlugosc_slowa);
	for (unsigned int i = POCZATEK; i <= KONIEC; i++)
		zrob_wiersz(i, dlugosc_slowa, tablica_identyfikatorow,
			    ostatni_zuzyty);
}

void przypisz_identyfikatory(char *slowo,
			     unsigned int **tablica_identyfikatorow,
			     unsigned int dlugosc_slowa)
{
	poczatkowy_ident<char> *tmp;
	tmp = new poczatkowy_ident<char>[dlugosc_slowa];
	for (unsigned int i = 0; i < dlugosc_slowa; i++) {
		tmp[i].zawartosc = slowo[i];
		tmp[i].pozycja = i;
	}
	std::sort(tmp, tmp + dlugosc_slowa);
	unsigned int ostatni = 0;
	przepisz_identyfikatory(tmp, tablica_identyfikatorow, dlugosc_slowa,
				ostatni, 0);
	delete[] tmp;
	przypisz_reszte_identyfikatorow(tablica_identyfikatorow,
					dlugosc_slowa, ostatni);
}

std::pair<unsigned int, unsigned int> identyfikator(unsigned int poczatek,
						    unsigned int dlugosc,
						    unsigned int **identyfikatory)
{
	std::pair<unsigned int, unsigned int> wynik;
	const unsigned int sztuczny_koniec = poczatek + dlugosc;
	const unsigned int LOG_PODLOGA = log_podloga(dlugosc);
	if (poczatek == sztuczny_koniec) {
		wynik.first = identyfikatory[poczatek][0];
		wynik.second = wynik.first;
	} else {
		wynik.first = identyfikatory[poczatek][LOG_PODLOGA];
		wynik.second = identyfikatory[sztuczny_koniec -
					      potega(LOG_PODLOGA)][LOG_PODLOGA];
	}
	return wynik;
}

void obsluz_zapytanie(unsigned int pocz1, unsigned int kon1,
		      unsigned int pocz2, unsigned int kon2,
		      unsigned int **tablica_identyfikatorow)
{
	unsigned int dlugosc_przedzialu = std::min(kon1 - pocz1,
						   kon2 - pocz2);
	dlugosc_przedzialu++;
	const std::pair<unsigned int, unsigned int> id1
		= identyfikator(pocz1, dlugosc_przedzialu,
			    tablica_identyfikatorow);
	const std::pair<unsigned int, unsigned int> id2 =
		identyfikator(pocz2, dlugosc_przedzialu,
			    tablica_identyfikatorow);
	if (id1 == id2) {
		if (kon1 - pocz1 == kon2 - pocz2) {
			printf("=\n");
			return;
		}
		if (kon1 - pocz1 < kon2 - pocz2) {
			printf("<\n");
			return;
		}
		printf(">\n");
		return;
	}
	if (id1 < id2) {
		printf("<\n");
	} else {
		printf(">\n");
	}
}

int main()
{
	unsigned int dlugosc_slowa, liczba_zapytan;
	char *slowo;
	unsigned int **tablica_identyfikatorow;
	scanf("%u%u", &dlugosc_slowa, &liczba_zapytan);
	slowo = new char[dlugosc_slowa];
	scanf("%c", slowo);
	for (unsigned int i = 0; i < dlugosc_slowa; i++) {
		scanf("%c", slowo + i);
	}
	const unsigned int LOG_PODLOGA = log_podloga(dlugosc_slowa);
	tablica_identyfikatorow = new unsigned int*[dlugosc_slowa];
	for (unsigned int i = 0; i < dlugosc_slowa; i++) {
		tablica_identyfikatorow[i] = new unsigned int[LOG_PODLOGA + 1];
	}
	przypisz_identyfikatory(slowo, tablica_identyfikatorow, dlugosc_slowa);
	for (unsigned int i = 0; i < liczba_zapytan; i++) {
		unsigned int pocz1, pocz2, kon1, kon2;
		scanf("%i%i%i%i", &pocz1, &kon1, &pocz2, &kon2);
		obsluz_zapytanie(pocz1 - 1, kon1 - 1, pocz2 - 1, kon2 - 1,
				 tablica_identyfikatorow);
	}
	delete[] slowo;
	for (unsigned int i = 0; i < dlugosc_slowa; i++)
		delete[] tablica_identyfikatorow[i];
	delete[] tablica_identyfikatorow;
	return 0;
}
